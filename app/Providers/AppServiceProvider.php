<?php

namespace App\Providers;

use App\Models\BlogCategory;
use App\Repositories\BlogCategoryRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('blog.admin.categories.includes.item_edit_main_col', function (View $view) {
            $categoryList = (new BlogCategoryRepository())->getForComboBox();
            $view->with('categoryList', $categoryList);
        });
    }
}
