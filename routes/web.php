<?php

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();


Route::group(['namespace' => 'Blog', 'prefix' => 'blog'], function () {

    Route::resource('posts', 'PostController')->names('blog.posts');

});

//Админка блога
$groupData = [
    'namespace' => 'Blog\Admin',
    'prefix' => 'admin/blog'
];
Route::group($groupData, function () {

    //Blog categories
    Route::resource('categories', 'CategoryController')
        ->only(['index', 'create', 'store', 'edit', 'update'])
        ->names('blog.admin.categories');
});
