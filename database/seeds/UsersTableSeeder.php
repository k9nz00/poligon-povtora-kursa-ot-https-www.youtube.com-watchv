<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Автор не известен',
                'email' => 'author_unknown@g.g',
                'password' => Hash::make('qwe123456')
            ],
            [
                'name' => 'Автор',
                'email' => 'author@g.g',
                'password' => Hash::make('qwe123456')
            ],
        ];

        DB::table('users')->insert($data);
    }
}
